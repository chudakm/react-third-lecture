import { IUserModel } from "../db/models/user";
import { UserRepository } from "../repositories";

export class UserService {
  constructor(private userRepository: UserRepository) {}

  public getAll() {
    return this.userRepository.getAll();
  }

  public getById(id: string) {
    const user = this.userRepository.getById(id);
    if (!user) throw new Error(`User with id ${id} not found.`);

    return user;
  }

  public getByUsername(username: string) {
    const user = this.userRepository.getByUsername(username);
    if (!user) return null;

    return user;
  }

  public create(dto: Partial<IUserModel>) {
    dto.avatar = "https://static.toiimg.com/photo/msid-67586673/67586673.jpg?3918697";

    return this.userRepository.create(dto);
  }

  public updateById(id: string, entity: Partial<IUserModel>) {
    const user = this.userRepository.getById(id);
    if (!user) throw new Error(`User with id ${id} not found.`);

    return this.userRepository.update(id, entity);
  }

  public deleteById(id: string) {
    const user = this.userRepository.getById(id);
    if (!user) throw new Error(`User with id ${id} not found.`);

    return this.userRepository.delete(id);
  }
}

export const userService = new UserService(new UserRepository());
