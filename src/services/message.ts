import { IMessageModel } from "../db/models/message";
import { IUserModel } from "../db/models/user";
import { MessageRepository } from "../repositories";
import { userService, UserService } from "./user";

export class MessageService {
  constructor(private messageRepository: MessageRepository, private userService: UserService) {}

  public getAll(): Array<IMessageModel & { user: IUserModel }> {
    return this.messageRepository.getAll().map(message => {
      return { ...message, user: this.userService.getById(message.userId) };
    });
  }

  public create(dto: Partial<IMessageModel>): IMessageModel & { user: IUserModel } {
    const message = this.messageRepository.create(dto);
    message.likedBy = [];
    return { ...message, user: this.userService.getById(message.userId) };
  }

  public like(id: string, { userId }) {
    const message = this.messageRepository.getById(id);

    if (message.likedBy.includes(userId)) {
      const userIdIndex = message.likedBy.findIndex(by => by === userId);
      message.likedBy.splice(userIdIndex, 1);
    } else {
      message.likedBy.push(userId);
    }

    return message;
  }

  public updateById(id: string, entity: Partial<IMessageModel>) {
    const message = this.messageRepository.getById(id);
    if (!message) throw new Error(`Message with id ${id} not found.`);

    return this.messageRepository.update(id, entity);
  }

  public deleteById(id: string) {
    const message = this.messageRepository.getById(id);
    if (!message) throw new Error(`Message with id ${id} not found.`);

    return this.messageRepository.delete(id);
  }
}

export const messageService = new MessageService(new MessageRepository(), userService);
