import { userService, UserService } from "./user";

export class AuthService {
  constructor(private userService: UserService) {}

  public login(dto: { username: string; password: string }) {
    const { username, password } = dto;
    const user = this.userService.getByUsername(username);
    if (!user) throw Error("User not found");
    if (user.password !== password) throw Error("Invalid password");

    return user;
  }
}

export const authService = new AuthService(userService);
