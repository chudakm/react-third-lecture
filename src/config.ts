import path from "path";

export const STATIC_PATH = path.resolve("client/build");
export const REACT_INDEX_PATH = path.resolve("client/build/index.html");

export const PORT = process.env.PORT || 3002;
