import { NextFunction, Request, Response, Router } from "express";
import { messageService } from "../services/message";

export const messagesRouter = Router();

messagesRouter.post("/", (req: Request, res: Response, next: NextFunction) => {
  const message = messageService.create(req.body);
  res.send(message);
});

messagesRouter.get("/", (req: Request, res: Response, next: NextFunction) => {
  const messages = messageService.getAll();
  res.send(messages);
});

messagesRouter.patch("/like/:id", (req: Request, res: Response) => {
  const messageId = req.params.id;
  const message = messageService.like(messageId, req.body);
  res.send(message);
});

messagesRouter.patch("/:id", (req: Request, res: Response, next: NextFunction) => {
  const messageId = req.params.id;
  const message = messageService.updateById(messageId, req.body);
  res.send(message);
});

messagesRouter.delete("/:id", (req: Request, res: Response, next: NextFunction) => {
  const messageId = req.params.id;
  const message = messageService.deleteById(messageId);
  res.send(message);
});
