import { NextFunction, Request, Response, Router } from "express";
import { userService } from "../services";

export const usersRouter = Router();

usersRouter.post("/", (req: Request, res: Response, next: NextFunction) => {
  try {
    const user = userService.create(req.body);
    res.send(user);
  } catch (err) {
    next(err);
  }
});

usersRouter.get("/", (req: Request, res: Response, next: NextFunction) => {
  const users = userService.getAll();
  res.send(users);
});

usersRouter.patch("/:id", (req: Request, res: Response, next: NextFunction) => {
  const userId = req.params.id;
  const user = userService.updateById(userId, req.body);
  res.send(user);
});

usersRouter.delete("/:id", (req: Request, res: Response, next: NextFunction) => {
  const userId = req.params.id;
  const user = userService.deleteById(userId);
  res.send(user);
});
