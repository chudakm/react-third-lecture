import { Express } from "express";
import { authRouter } from "./auth";
import { messagesRouter } from "./messages";
import { usersRouter } from "./user";

export const routes = (app: Express) => {
  app.use("/api/auth", authRouter);
  app.use("/api/users", usersRouter);
  app.use("/api/messages", messagesRouter);
};
