import { NextFunction, Request, Response, Router } from "express";
import { authService } from "../services";

export const authRouter = Router();

authRouter.post("/login", (req: Request, res: Response, next: NextFunction) => {
  try {
    const user = authService.login(req.body);
    res.send(user);
  } catch (err) {
    next(err);
  }
});
