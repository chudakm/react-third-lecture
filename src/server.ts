import express, { NextFunction, Request, Response } from "express";
import http from "http";
import { REACT_INDEX_PATH, PORT, STATIC_PATH } from "./config";
import { routes } from "./routes";
import cors from "cors";

const app = express();
const httpServer = new http.Server(app);

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static(STATIC_PATH));

routes(app);

app.use((_: Request, res: Response) => {
  res.sendFile(REACT_INDEX_PATH);
});

app.use(function (err: Error, req: Request, res: Response, next: NextFunction) {
  res.status(400);
  res.send({ error: err.message });
});

httpServer.listen(PORT, () => {
  console.log(`Listen server on port ${PORT}`);
});
