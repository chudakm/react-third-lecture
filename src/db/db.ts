import { IMessageModel } from "./models/message";
import { IUserModel } from "./models/user";

export interface IDbStructure {
  users: IUserModel[];
  messages: IMessageModel[];
}
export type IDbCollections = IUserModel[] | IMessageModel[];
export type IDbCollectionTypes = IUserModel | IMessageModel;

export const dbAdapter: IDbStructure = {
  users: [
    {
      id: "1",
      avatar: "https://static.toiimg.com/photo/msid-67586673/67586673.jpg?3918697",
      username: "admin",
      password: "admin",
      isAdmin: true,
      createdAt: new Date(),
    },
    {
      id: "2",
      avatar: "https://static.toiimg.com/photo/msid-67586673/67586673.jpg?3918697",
      username: "user",
      password: "user",
      isAdmin: false,
      createdAt: new Date(),
    },
  ],
  messages: [
    {
      id: "1",
      userId: "1",
      text: "Some text",
      likedBy: ["1"],
      createdAt: new Date(),
    },
  ],
};
