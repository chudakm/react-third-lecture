import { IBaseModel } from "./base";

export interface IUserModel extends IBaseModel {
  id: string;
  avatar: string;
  username: string;
  password: string;
  isAdmin: boolean;
}
