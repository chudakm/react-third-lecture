import { IBaseModel } from "./base";
import { IUserModel } from "./user";

export interface IMessageModel extends IBaseModel {
  id: string;
  userId: string;
  text: string;
  likedBy: string[];
}
