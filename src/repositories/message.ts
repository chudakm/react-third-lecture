import { IMessageModel } from "../db/models/message";
import { BaseRepository } from "./base";

export class MessageRepository extends BaseRepository<IMessageModel> {
  constructor() {
    super("messages");
  }
}
