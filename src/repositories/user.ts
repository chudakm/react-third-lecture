import { IUserModel } from "../db/models/user";
import { BaseRepository } from "./base";

export class UserRepository extends BaseRepository<IUserModel> {
  constructor() {
    super("users");
  }

  public getByUsername(username: string) {
    return this.dbContext.find(user => user.username === username);
  }
}
