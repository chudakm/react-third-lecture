import { dbAdapter, IDbCollectionTypes, IDbStructure } from "../db";
import { v4 } from "uuid";

export class BaseRepository<T extends IDbCollectionTypes> {
  protected dbContext: T[];

  constructor(collectionName: keyof IDbStructure) {
    // @ts-ignore
    this.dbContext = dbAdapter[collectionName];
  }

  public generateId(): string {
    return v4();
  }

  public getAll() {
    return this.dbContext;
  }

  public getById(id: string) {
    return this.dbContext.find(it => it.id === id);
  }

  public create(data: Partial<T>) {
    data.id = this.generateId();
    data.createdAt = new Date();

    this.dbContext.push(data as T);
    return this.dbContext.find(it => it.id === data.id);
  }

  public update(id: string, dataToUpdate: Partial<T>) {
    dataToUpdate.updatedAt = new Date();
    const record = this.dbContext.find(it => it.id === id);
    Object.assign(record, dataToUpdate);

    return record;
  }

  public delete(id: string) {
    const recordId = this.dbContext.findIndex(it => it.id === id);
    const [deleted] = this.dbContext.splice(recordId, 1);
    return deleted;
  }
}
