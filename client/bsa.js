import { Chat } from "./src/components/chat/chat";
import { rootReducer } from "./src/store/store";

export default { Chat, rootReducer };
