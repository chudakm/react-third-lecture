export interface IMessage {
  id: string;
  user: IUser;
  userId: string;
  text: string;
  createdAt: string;
  editedAt?: Date;
  likedBy: string[];
}

export interface IUser {
  id: string;
  avatar: string;
  username: string;
  password: string;
  isAdmin: boolean;
}

export interface IGroupedMessages {
  [group: string]: IMessage[];
}

export interface ILoginCredentials {
  username: string;
  password: string;
}
