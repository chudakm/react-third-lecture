import React from "react";
import { Chat, Login, UserEditor } from "./components";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import { UserList } from "./components/user-list";
import ProtectedRoute from "./components/protected-route/protected-route";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { MessageEditor } from "./components/message-editor/message-editor";

export const App = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route path='/login'>
          <Login />
        </Route>

        <Route path='/user-list' render={() => <ProtectedRoute component={UserList} />} />
        <Route path='/user-editor' render={() => <ProtectedRoute component={UserEditor} />} />
        <Route path='/chat' render={() => <ProtectedRoute component={Chat} />} />
        <Route path='/message-editor' render={() => <ProtectedRoute component={MessageEditor} />} />

        <Redirect to='/login' />
      </Switch>
      <ToastContainer />
    </BrowserRouter>
  );
};
