import { IUser } from "../../types";

export class UserApi {
  private static BASE_URL = `api/users/`;

  public static async getAll() {
    const response = await fetch(this.BASE_URL);
    return response.json();
  }

  public static async create(user: Partial<IUser>) {
    const response = await fetch(this.BASE_URL, {
      method: "POST",
      body: JSON.stringify(user),
      headers: { "Content-Type": "application/json" },
    });
    return response.json();
  }

  public static async update(id: string, user: Partial<IUser>) {
    const response = await fetch(this.BASE_URL + id, {
      method: "PATCH",
      body: JSON.stringify(user),
      headers: { "Content-Type": "application/json" },
    });

    return response.json();
  }

  public static async delete(id: string) {
    const response = await fetch(this.BASE_URL + id, { method: "DELETE" });
    return response.json();
  }
}
