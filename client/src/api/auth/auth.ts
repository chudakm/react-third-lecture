import { ILoginCredentials } from "../../types";

export class AuthApi {
  private static BASE_URL = `api/auth/`;

  public static async login(credentials: ILoginCredentials) {
    const response = await fetch(this.BASE_URL + "login", {
      method: "POST",
      body: JSON.stringify(credentials),
      headers: {
        "Content-Type": "application/json",
      },
    });
    const body = await response.json();
    if (body.error) {
      throw new Error(body.error);
    }

    return body;
  }
}
