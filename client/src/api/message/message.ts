import { IMessage } from "../../types";

export class MessageApi {
  private static BASE_URL = "api/messages/";

  public static async getAll(): Promise<IMessage[]> {
    const response = await fetch(this.BASE_URL);
    return response.json();
  }

  public static async create(message: Partial<IMessage>) {
    const response = await fetch(this.BASE_URL, {
      method: "POST",
      body: JSON.stringify(message),
      headers: { "Content-Type": "application/json" },
    });

    return response.json();
  }

  public static async update(id: string, message: Partial<IMessage>) {
    const response = await fetch(this.BASE_URL + id, {
      method: "PATCH",
      body: JSON.stringify(message),
      headers: { "Content-Type": "application/json" },
    });

    return response.json();
  }

  public static async like(id: string, dto: { userId: string }) {
    const response = await fetch(this.BASE_URL + "like/" + id, {
      method: "PATCH",
      body: JSON.stringify(dto),
      headers: { "Content-Type": "application/json" },
    });

    return response.json();
  }

  public static async delete(id: string) {
    const response = await fetch(this.BASE_URL + id, {
      method: "DELETE",
    });

    return response.json();
  }
}
