import { createAsyncThunk, createEntityAdapter, createSelector, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { toast } from "react-toastify";
import { UserApi } from "../api";
import { IUser } from "../types";
import { RootState } from "./store";

export const loadUsers = createAsyncThunk("load-users", async _ => {
  return UserApi.getAll();
});

export const addUser = createAsyncThunk("add-user", async (user: Partial<IUser>) => {
  const createdUser = await UserApi.create(user);
  toast("User added", { type: "success" });
  return createdUser;
});

export const editUser = createAsyncThunk("edit-user", async ({ id, user }: { id: string; user: Partial<IUser> }) => {
  const updatedUser = UserApi.update(id, user);
  toast("User updated", { type: "success" });
  return updatedUser;
});

export const deleteUser = createAsyncThunk("delete-user", async (id: string) => {
  return UserApi.delete(id);
});

const usersAdapter = createEntityAdapter<IUser>();

export const usersSlice = createSlice({
  name: "users",
  initialState: usersAdapter.getInitialState({ isLoading: false, editUserId: "" }),
  reducers: {
    updateEditUserId: (state, action: PayloadAction<string>) => {
      state.editUserId = action.payload;
    },
  },
  extraReducers: builder => {
    builder
      .addCase(loadUsers.pending, state => {
        state.isLoading = true;
      })
      .addCase(loadUsers.fulfilled, (state, action: PayloadAction<IUser[]>) => {
        state.isLoading = false;
        usersAdapter.setAll(state, action);
      })
      .addCase(loadUsers.rejected, state => {
        state.isLoading = false;
      })
      .addCase(deleteUser.fulfilled, (state, action: PayloadAction<IUser>) => {
        usersAdapter.removeOne(state, action.payload.id);
      })
      .addCase(addUser.fulfilled, (state, action: PayloadAction<IUser>) => {
        usersAdapter.addOne(state, action.payload);
      })
      .addCase(editUser.fulfilled, (state, action: PayloadAction<IUser>) => {
        usersAdapter.upsertOne(state, action.payload);
      });
  },
});

export const { updateEditUserId } = usersSlice.actions;

const selectUsers = (state: RootState) => state.users;
export const selectIsUsersLoading = createSelector(selectUsers, users => users.isLoading);
export const selectEditUserId = createSelector(selectUsers, users => users.editUserId);
export const usersSelectors = usersAdapter.getSelectors<RootState>(selectUsers);

export const usersReducer = usersSlice.reducer;
