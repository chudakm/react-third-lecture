import { combineReducers, configureStore } from "@reduxjs/toolkit";
import { getDefaultMiddleware } from "@reduxjs/toolkit";
import { userReducer } from "./user-slice";
import { usersReducer } from "./users-slice";
import { messagesReducer } from "./messages-slice";

const customizedMiddleware = getDefaultMiddleware({
  serializableCheck: false,
});

export const rootReducer = combineReducers({
  user: userReducer,
  users: usersReducer,
  messages: messagesReducer,
});

export const store = configureStore({
  reducer: rootReducer,
  middleware: customizedMiddleware,
});

export type RootState = ReturnType<typeof store.getState>;
