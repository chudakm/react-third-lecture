import { createAsyncThunk, createEntityAdapter, createSelector, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { toast } from "react-toastify";
import { MessageApi } from "../api";
import { IMessage } from "../types";
import { RootState } from "./store";

export const loadMessages = createAsyncThunk("load-messages", async () => {
  return MessageApi.getAll();
});

export const addMessage = createAsyncThunk("add-message", async (_, thunkApi) => {
  const state = thunkApi.getState() as RootState;
  const userId = state.user.id;
  const text = state.messages.inputText;

  return MessageApi.create({ userId, text });
});

export const likeMessage = createAsyncThunk("like-message", async (messageId: string, thunkApi) => {
  const state = thunkApi.getState() as RootState;
  const userId = state.user.id;

  return MessageApi.like(messageId, { userId });
});

export const deleteMessage = createAsyncThunk("delete-message", async (messageId: string) => {
  return MessageApi.delete(messageId);
});

export const updateMessage = createAsyncThunk("update-message", async (text: string, thunkApi) => {
  const state = thunkApi.getState() as RootState;
  const messageId = state.messages.editMessageId;

  return MessageApi.update(messageId, { text });
});

const messagesAdapter = createEntityAdapter<IMessage>();

export const messagesSlice = createSlice({
  name: "messages",
  initialState: messagesAdapter.getInitialState({ isLoading: false, editMessageId: "", inputText: "" }),
  reducers: {
    updateEditMessageId: (state, action: PayloadAction<string>) => {
      state.editMessageId = action.payload;
    },
    updateInputText: (state, action: PayloadAction<string>) => {
      state.inputText = action.payload;
    },
    cancelEdit: state => {
      state.editMessageId = "";
    },
  },
  extraReducers: builder => {
    builder
      .addCase(loadMessages.pending, state => {
        state.isLoading = true;
      })
      .addCase(loadMessages.fulfilled, (state, action: PayloadAction<IMessage[]>) => {
        state.isLoading = false;
        messagesAdapter.setAll(state, action);
      })
      .addCase(loadMessages.rejected, state => {
        state.isLoading = false;
      })
      .addCase(addMessage.fulfilled, (state, action: PayloadAction<IMessage>) => {
        state.inputText = "";
        messagesAdapter.upsertOne(state, action.payload);
      })
      .addCase(likeMessage.fulfilled, (state, action: PayloadAction<IMessage>) => {
        messagesAdapter.upsertOne(state, action.payload);
      })
      .addCase(deleteMessage.fulfilled, (state, action: PayloadAction<IMessage>) => {
        messagesAdapter.removeOne(state, action.payload.id);
      })
      .addCase(updateMessage.fulfilled, (state, action: PayloadAction<IMessage>) => {
        state.editMessageId = "";
        console.log(action.payload);
        messagesAdapter.upsertOne(state, action.payload);
      });
  },
});

export const { updateEditMessageId, updateInputText, cancelEdit } = messagesSlice.actions;

const selectMessages = (state: RootState) => state.messages;
export const selectIsMessagesLoading = createSelector(selectMessages, messages => messages.isLoading);
export const selectEditMessageId = createSelector(selectMessages, messages => messages.editMessageId);
export const selectParticipantsCount = createSelector(selectMessages, messages => {
  const userIds = messages.ids.map(key => {
    const message = messages.entities[key];
    return message?.userId;
  });

  return new Set(userIds).size;
});
export const selectLastMessageAt = createSelector(selectMessages, messages => {
  const { entities, ids } = messages;

  if (ids.length) {
    // @ts-ignore
    return entities[ids[ids.length - 1]].createdAt;
  } else return new Date();
});
export const selectInputText = createSelector(selectMessages, messages => messages.inputText);

export const messagesSelectors = messagesAdapter.getSelectors<RootState>(selectMessages);

export const messagesReducer = messagesSlice.reducer;
