import { createSelector, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { IUser } from "../types";
import { RootState } from "./store";

interface IUserState extends IUser {}

const initialState: IUserState = {
  id: "",
  avatar: "",
  username: "",
  password: "",
  isAdmin: false,
};

export const userSlice = createSlice({
  name: "user",
  initialState: initialState,
  reducers: {
    updateUser: (state, action: PayloadAction<IUser>) => {
      return { ...state, ...action.payload };
    },
  },
});

export const { updateUser } = userSlice.actions;

const selectUser = (state: RootState) => state.user;
export const selectUserId = createSelector(selectUser, user => user.id);

export const userReducer = userSlice.reducer;
