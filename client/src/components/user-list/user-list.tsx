import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { deleteUser, selectIsUsersLoading, updateEditUserId, usersSelectors } from "../../store/users-slice";
import { Preloader } from "../chat/components/preloader";
import "./user-list.scss";

interface IUserListProps {}

export const UserList: React.FC<IUserListProps> = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const isUsersLoading = useSelector(selectIsUsersLoading);
  const users = useSelector(usersSelectors.selectAll);

  const onAddUser = () => {
    dispatch(updateEditUserId(""));
    history.push("user-editor");
  };

  const onEditUser = (id: string) => {
    dispatch(updateEditUserId(id));
    history.push("user-editor");
  };

  const onDeleteUser = (id: string) => {
    dispatch(deleteUser(id));
  };

  if (isUsersLoading) {
    return (
      <div className='user-list'>
        <Preloader />
      </div>
    );
  }

  return (
    <div className='user-list'>
      <div className='container'>
        <button className='add-user' onClick={onAddUser}>
          Add User
        </button>

        {users.map(user => {
          return (
            <div className='user-card' key={user.id}>
              <div className='username'>{user.username}</div>

              <div className='controls'>
                <button onClick={() => onEditUser(user.id)}>Edit</button>

                <button className='delete-user' onClick={() => onDeleteUser(user.id)}>
                  Delete
                </button>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};
