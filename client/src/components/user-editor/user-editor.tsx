import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { addUser, editUser, selectEditUserId, usersSelectors } from "../../store/users-slice";
import "./user-editor.scss";

interface IUserEditorProps {}

export const UserEditor: React.FC<IUserEditorProps> = () => {
  const dispatch = useDispatch();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [isAdmin, setIsAdmin] = useState(false);
  const editUserId = useSelector(selectEditUserId);
  const users = useSelector(usersSelectors.selectAll);

  useEffect(() => {
    if (editUserId) {
      const user = users.find(user => user.id === editUserId);
      if (user) {
        setUsername(user.username);
        setPassword(user.password);
        setIsAdmin(user.isAdmin);
      }
    } else {
      setUsername("");
      setPassword("");
      setIsAdmin(false);
    }
  }, [editUserId, users]);

  const onUsernameInput = (e: React.FormEvent<HTMLInputElement>) => {
    setUsername(e.currentTarget.value);
  };

  const onPasswordInput = (e: React.FormEvent<HTMLInputElement>) => {
    setPassword(e.currentTarget.value);
  };

  const onIsAdminInput = () => {
    setIsAdmin(!isAdmin);
  };

  const onSave = () => {
    if (!editUserId) {
      dispatch(addUser({ username, password, isAdmin }));
    } else {
      dispatch(editUser({ id: editUserId, user: { username, password, isAdmin } }));
    }
  };

  return (
    <div className='user-editor'>
      <div className='header'>
        <div>Edit User</div>

        <div className='nav'>
          <Link to={"user-list"}> User List </Link>
        </div>
      </div>

      <div className='form'>
        <label htmlFor='username'>Username</label>
        <input id='username' type='text' value={username} onInput={onUsernameInput} />

        <br />

        <label htmlFor='password'>Password</label>
        <input id='password' type='text' value={password} onInput={onPasswordInput} />

        <br />

        <label htmlFor='isAdmin'>Is Admin</label>
        <input id='isAdmin' type='checkbox' checked={isAdmin} onChange={onIsAdminInput} />
      </div>

      <button className='save' onClick={onSave}>
        Save
      </button>
    </div>
  );
};
