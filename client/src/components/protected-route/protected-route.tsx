import React from "react";
import { useSelector } from "react-redux";
import { Redirect } from "react-router-dom";
import { selectUserId } from "../../store/user-slice";

interface ProtectedRouteProps {
  component: React.FC<any>;
}

const ProtectedRoute: React.FC<ProtectedRouteProps> = ({ component: Component }) => {
  const isLoggedIn = useSelector(selectUserId);

  return !!isLoggedIn ? (
    <Component />
  ) : (
    <Redirect
      to={{
        pathname: "/login",
      }}
    />
  );
};

export default ProtectedRoute;
