import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { messagesSelectors, selectEditMessageId, cancelEdit, updateMessage } from "../../store/messages-slice";
import "./message-editor.scss";

interface IMessageEditorProps {}

export const MessageEditor: React.FC<IMessageEditorProps> = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const editMessageId = useSelector(selectEditMessageId);
  const messages = useSelector(messagesSelectors.selectAll);
  const [text, setText] = useState("");

  useEffect(() => {
    const message = messages.find(message => message.id === editMessageId);
    if (message) {
      setText(message.text);
    }
  }, [editMessageId]);

  const onMessageInput = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    setText(e.currentTarget.value);
  };

  const onSave = () => {
    dispatch(updateMessage(text));
    setText("");
    history.push("/chat");
  };

  const onCancel = () => {
    dispatch(cancelEdit());
    history.push("/chat");
  };

  return (
    <div className={`edit-message-modal modal-shown`} role='dialog'>
      <span className='title'>Edit message</span>

      <textarea className='edit-message-input' cols={70} rows={10} value={text} onInput={onMessageInput}></textarea>
      <br />

      <div className='controls'>
        <button className='edit-message-button' onClick={onSave}>
          SAVE
        </button>
        <button className='edit-message-close' onClick={onCancel}>
          CANCEL
        </button>
      </div>
    </div>
  );
};
