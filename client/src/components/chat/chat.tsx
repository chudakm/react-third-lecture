import React from "react";
import "./chat.scss";
import { Header, MessageList, MessageInput } from "./components";
import { Preloader } from "./components/preloader";
import { useSelector } from "react-redux";
import { selectIsMessagesLoading } from "../../store/messages-slice";

interface IChatProps {}

export const Chat: React.FC<IChatProps> = () => {
  const preloader = useSelector(selectIsMessagesLoading);

  if (preloader) {
    return <Preloader />;
  }

  return (
    <div className='chat'>
      <Header />
      <br />
      <MessageList />
      <br />
      <MessageInput />
    </div>
  );
};
