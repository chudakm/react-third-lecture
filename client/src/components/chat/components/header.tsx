import React from "react";
import { useSelector } from "react-redux";
import { messagesSelectors, selectLastMessageAt, selectParticipantsCount } from "../../../store/messages-slice";
import "../chat.scss";

interface IHeaderProps {}

export const Header: React.FC<IHeaderProps> = () => {
  const chatName = "BSA Chat";
  const participantsCount = useSelector(selectParticipantsCount);
  const messagesCount = useSelector(messagesSelectors.selectTotal);
  const lastMessageAt = useSelector(selectLastMessageAt);

  return (
    <div className='header'>
      <div className='header-info'>
        <span className='header-title'>{chatName}</span>
        <span className='header-users-count'>{participantsCount}</span>
        <span className='header-messages-count'>{messagesCount}</span>
      </div>

      <span className='header-last-message-date'>{lastMessageTime(new Date(lastMessageAt))}</span>
    </div>
  );
};

const lastMessageTime = (lastMessageAt: Date) => {
  const day = lastMessageAt.getDate();
  const month = lastMessageAt.getMonth();
  const year = lastMessageAt.getFullYear();

  const hour = lastMessageAt.getHours();
  const minutes = lastMessageAt.getMinutes();

  return `${day < 10 ? "0" : ""}${day}.${month < 10 ? "0" : ""}${month + 1}.${year} ${hour}:${minutes < 10 ? "0" : ""}${minutes}`;
};
