import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { addMessage, selectInputText, updateInputText } from "../../../store/messages-slice";
import "../chat.scss";

interface IMessageInputProps {}

export const MessageInput: React.FC<IMessageInputProps> = () => {
  const dispatch = useDispatch();
  const text = useSelector(selectInputText);

  const onMessageInput = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    dispatch(updateInputText(e.target.value));
  };

  const onSend = () => {
    dispatch(addMessage());
  };

  const onKeyPress = (e: React.KeyboardEvent<HTMLTextAreaElement>) => {
    if (e.key === "Enter") onSend();
  };

  return (
    <div className='message-input'>
      <textarea className='message-input-text' cols={100} rows={5} onInput={onMessageInput} value={text} onKeyPress={onKeyPress}></textarea>

      <button className='message-input-button' onClick={onSend} disabled={!!!text}>
        Send
      </button>
    </div>
  );
};
