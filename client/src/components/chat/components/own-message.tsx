import React from "react";
import "../chat.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCog, faTrash } from "@fortawesome/free-solid-svg-icons";
import { dateToHoursAndMinutes } from "../../../util/date-to-hours-and-minutes";
import { useDispatch } from "react-redux";
import { deleteMessage, updateEditMessageId } from "../../../store/messages-slice";
import { useHistory } from "react-router-dom";

interface IOwnMessageProps {
  id: string;
  createdAt: string;
  text: string;
}

export const OwnMessage: React.FC<IOwnMessageProps> = ({ id, createdAt, text }) => {
  const history = useHistory();
  const dispatch = useDispatch();

  const onEdit = () => {
    dispatch(updateEditMessageId(id));
    history.push("/message-editor");
  };

  const onDelete = () => {
    dispatch(deleteMessage(id));
  };

  return (
    <div className='own-message'>
      <span className='message-time'>{dateToHoursAndMinutes(new Date(createdAt))}</span>
      <FontAwesomeIcon className='message-edit' icon={faCog} onClick={onEdit}></FontAwesomeIcon>
      <FontAwesomeIcon className='message-delete' icon={faTrash} onClick={onDelete}></FontAwesomeIcon>
      <br />

      <span className='message-text'>{text}</span>
    </div>
  );
};
