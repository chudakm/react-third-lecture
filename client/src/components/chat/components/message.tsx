import React from "react";
import classNames from "classnames";
import "../chat.scss";
import { faHeart } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { dateToHoursAndMinutes } from "../../../util/date-to-hours-and-minutes";
import { useDispatch, useSelector } from "react-redux";
import { IMessage } from "../../../types";
import { selectUserId } from "../../../store/user-slice";
import { likeMessage } from "../../../store/messages-slice";

interface IMessageProps extends IMessage {}

export const Message: React.FC<IMessageProps> = ({ id, text, createdAt, user, likedBy }) => {
  const dispatch = useDispatch();
  const userId = useSelector(selectUserId);

  const onLike = () => dispatch(likeMessage(id));

  return (
    <div className='message'>
      <div className='message-left'>
        <div className='message-user-avatar'>
          <img src={user.avatar} alt='avatar' />
        </div>
      </div>

      <div className='message-right'>
        <span className='message-user-name'>{user.username}</span>
        <span className='message-time'>{dateToHoursAndMinutes(new Date(createdAt))}</span>
        <FontAwesomeIcon
          className={classNames("like", { "message-like": !likedBy.includes(userId), "message-liked": likedBy.includes(userId) })}
          onClick={onLike}
          icon={faHeart}
        />

        <br />

        <span className='message-text'>{text}</span>
      </div>
    </div>
  );
};
