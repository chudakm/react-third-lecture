import React from "react";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { toast } from "react-toastify";
import { AuthApi } from "../../api";
import { loadMessages } from "../../store/messages-slice";
import { updateUser } from "../../store/user-slice";
import { loadUsers } from "../../store/users-slice";
import { IUser } from "../../types";
import "./login.scss";

interface ILoginProps {}

export const Login: React.FC<ILoginProps> = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [isLoggingIn, setIsLoggingIn] = useState(false);

  const onLoginInput = (e: React.FormEvent<HTMLInputElement>) => {
    setUsername(e.currentTarget.value);
  };

  const onPasswordInput = (e: React.FormEvent<HTMLInputElement>) => {
    setPassword(e.currentTarget.value);
  };

  const onLogin = () => {
    setIsLoggingIn(true);
    AuthApi.login({ username, password })
      .then((user: IUser) => {
        dispatch(updateUser(user));

        if (user.isAdmin) {
          dispatch(loadUsers());
          history.push("user-list");
        } else {
          dispatch(loadMessages());
          history.push("chat");
        }
      })
      .catch(err => {
        toast(err.message, { type: "error" });
      })
      .finally(() => setIsLoggingIn(false));
  };

  const isLoginDisabled = () => {
    return Boolean(!username || !password || isLoggingIn);
  };

  return (
    <div className='login'>
      <div className='form' onKeyPress={e => e.key === "Enter" && onLogin()}>
        <label>Username</label>
        <input className='login-input' type='text' value={username} onInput={onLoginInput} />

        <label>Password</label>
        <input className='password' type='password' value={password} onInput={onPasswordInput} />

        <button onClick={onLogin} disabled={isLoginDisabled()}>
          Login
        </button>
      </div>
    </div>
  );
};
